package ordination;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

public abstract class Ordination {
	private LocalDate startDen;
	private LocalDate slutDen;
	private Laegemiddel laegemiddel;

	public Ordination(LocalDate startDen, LocalDate slutDen) {
		this.startDen = startDen;
		this.slutDen = slutDen;
	}

	public LocalDate getStartDen() {
		return startDen;
	}

	public LocalDate getSlutDen() {
		return slutDen;
	}

	/**
	 * Antal hele dage mellem startdato og slutdato. Begge dage inklusive.
	 * <<<<<<< HEAD
	 *
	 * =======
	 * 
	 * >>>>>>> 4c40281298a7cd1819302ca6b9dab7d4fa25d3c8
	 * 
	 * @return antal dage ordinationen gælder for
	 */
	public int antalDage() {
		return (int) ChronoUnit.DAYS.between(startDen, slutDen) + 1;
	}

	@Override
	public String toString() {
		return startDen.toString();
	}

	/**
	 * Returnerer den totale dosis der er givet i den periode ordinationen er
	 * gyldig <<<<<<< HEAD
	 *
	 * =======
	 * 
	 * >>>>>>> 4c40281298a7cd1819302ca6b9dab7d4fa25d3c8
	 * 
	 * @return
	 */
	public abstract double samletDosis();

	/**
	 * Returnerer den gennemsnitlige dosis givet pr dag i den periode
	 * ordinationen er gyldig <<<<<<< HEAD
	 *
	 * =======
	 * 
	 * >>>>>>> 4c40281298a7cd1819302ca6b9dab7d4fa25d3c8
	 * 
	 * @return
	 */
	public abstract double doegnDosis();

	/**
	 * Returnerer ordinationstypen som en String <<<<<<< HEAD
	 *
	 * =======
	 * 
	 * >>>>>>> 4c40281298a7cd1819302ca6b9dab7d4fa25d3c8
	 * 
	 * @return
	 */
	public abstract String getType();

	public Laegemiddel getLaegemiddel() {
		return laegemiddel;
	}

	public void setLaegemiddel(Laegemiddel laegemiddel) {
		this.laegemiddel = laegemiddel;
	}

	public void clearLaegemiddel() {
		laegemiddel = null;
	}

}
