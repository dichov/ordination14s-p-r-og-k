package ordination;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;

public class PN extends Ordination {

	public PN(LocalDate startDen, LocalDate slutDen, double antalEnheder) {
		super(startDen, slutDen);
		this.antalEnheder = antalEnheder;
	}

	private double antalEnheder;
	ArrayList<LocalDate> datoer = new ArrayList<>();

	/**
	 * Registrerer at der er givet en dosis paa dagen givesDen Returnerer true
	 * hvis givesDen er inden for ordinationens gyldighedsperiode og datoen
	 * huskes Retrurner false ellers og datoen givesDen ignoreres
	 *
	 * @param givesDen
	 * @return
	 */
	public boolean givDosis(LocalDate givesDen) {
		boolean result = false;
		if (givesDen.isAfter(getStartDen()) && givesDen.isBefore(getSlutDen())) {
			result = true;
			datoer.add(givesDen);

		}
		return result;
	}

	@Override
	public double doegnDosis() {
		double result = (datoer.size() * getAntalEnheder())
				/ ChronoUnit.DAYS.between(getStartDen(), getSlutDen());
		return result;
	}

	@Override
	public double samletDosis() {
		double result = datoer.size() * getAntalEnheder();
		return result;
	}

	/**
	 * Returnerer antal gange ordinationen er anvendt
	 *
	 * @return
	 */
	public int getAntalGangeGivet() {
		return datoer.size();
	}

	public double getAntalEnheder() {
		return antalEnheder;
	}

	@Override
	public String getType() {
		return "PN";
	}

}
